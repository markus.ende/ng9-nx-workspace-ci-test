import { async, TestBed } from '@angular/core/testing';
import { SomelibWithKarmaTestsModule } from './somelib-with-karma-tests.module';

describe('SomelibWithKarmaTestsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SomelibWithKarmaTestsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SomelibWithKarmaTestsModule).toBeDefined();
  });
});
