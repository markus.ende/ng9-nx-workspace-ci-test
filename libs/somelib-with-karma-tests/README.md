# somelib-with-karma-tests

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test somelib-with-karma-tests` to execute the unit tests.
