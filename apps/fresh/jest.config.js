module.exports = {
  name: 'fresh',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/fresh',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
